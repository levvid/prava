package com.mlginc.gibsonlevvid.prava;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import android.Manifest;

import org.json.JSONObject;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.location.LocationListener;

import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


public class MapsActivity extends AppCompatActivity implements LocationListener, OnMapReadyCallback,
        ConnectionCallbacks, OnConnectionFailedListener {

    GoogleMap mMap;
    ArrayList<LatLng> markerPoints;
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    protected static final String TAG = "Trovami TAG: ";

    /**
     * User
     */
    private String mUserID, mUserPhonenumber, mUserPassword;
    private String PHONENUMBER = "phone_number", PASSWORD = "password";
    private EditText name, password, phoneNumber, name_login, password_login;
    private SharedPreferences sharedPref;
    private BackendlessUser currentUser;
    /***
     * Nav drawer
     */
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;
    /*****
     * Origin and destination variables
     *****/
    private LatLng origin = null, dest = null;

    /***
     * Merchants
     */
    String[] merchants = {"Jumia", "Kilimall", "Shop It", "Mama Mikes", "Mimi", "Electro Hub"};
    //Views
    ListView listView, listViewDeliveryLocations;
    SupportMapFragment mapFragment;
    ScrollView signup;
    ScrollView login;
    /***
     * Merchant images
     **/
    Integer[] imageId = {
            R.drawable.call,
            R.drawable.call,
            R.drawable.call,
            R.drawable.call,
            R.drawable.call,
            R.drawable.call,
            R.drawable.call,
            R.drawable.call

    };
    /***
     * Merchant list call and place order buttons
     */
    private Button callMerchant;
    private Button placeOrder;
    private TextView navUsername;


    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1000;
    private static final int MY_PERMISSIONS_REQUEST_CALL = 2000;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 500000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";
    private Button startNavigation, buttonAddDeliveryLocation;
    private Button buttonPackageDelivered;
    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    /**
     * Represents a geographical location.
     */
    protected Location mCurrentLocation;
    protected Location mTargetLocation;
    /**
     * Time when the location was updated represented as a String.
     */
    protected String mLastUpdateTime;

    /**
     * Phone number input
     **/
    private String recipientPhoneNumber = "";


    private TextView loginLink, signupLink;
    private Location mPackageLocation = new Location("");
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        setUpBackendless();

        setUpMerchantList();
        initializeDeliveryLocationsListView(); //buttons and Listview
        setUpInput();
        setUpNavigationAndDelivery();
        setUpActionBarAndNavigation();
        checkLogin();
        buildGoogleApiClient();
        //setUpMap();

        ///setUpRealTimeLocation(savedInstanceState);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    private void setUpNavigationAndDelivery() {
        startNavigation = (Button) findViewById(R.id.button_startnavigation);
        buttonPackageDelivered = (Button) findViewById(R.id.button_packagedelivered);
        startNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q="
                        + mTargetLocation.getLatitude() + "," + mTargetLocation.getLongitude()));
                // Make the Intent explicit by setting the Google Maps package
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mapIntent);
                }

            }
        });
        buttonPackageDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDeliveryStatus();
            }
        });
    }


    /**
     * Input
     */
    private void setUpInput() {
        name = (EditText) findViewById(R.id.input_name);
        password = (EditText) findViewById(R.id.input_password);
        name_login = (EditText) findViewById(R.id.inpt_phonenumber);
        password_login = (EditText) findViewById(R.id.inpt_password);
        phoneNumber = (EditText) findViewById(R.id.input_phonenumber);
        loginLink = (TextView) findViewById(R.id.link_login);
        loginLink.setPaintFlags(loginLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        signupLink = (TextView) findViewById(R.id.link_signup);
        signupLink.setPaintFlags(signupLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeViewInvisible(signup);
                makeViewVisible(login);
            }
        });
        signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeViewInvisible(login);
                makeViewVisible(signup);
            }
        });
        Button buttonLogin = (Button) findViewById(R.id.btn_login);
        Button buttonSignup = (Button) findViewById(R.id.btn_signup);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUserPassword = password_login.getText().toString();
                mUserPhonenumber = name_login.getText().toString();
                if (mUserPassword.trim().length() < 6) {
                    Toast.makeText(getBaseContext(), "Password must be more than 7 characters " + mUserPassword.trim().length(), Toast.LENGTH_SHORT).show();
                    mUserPassword = password_login.getText().toString();
                    if (mUserPassword.trim().length() < 6) {
                        Toast.makeText(getBaseContext(), "Please enter a valid password " + mUserPassword.trim().length(), Toast.LENGTH_SHORT).show();
                    }
                } else if (mUserPhonenumber.trim().length() < 8) {
                    Toast.makeText(getBaseContext(), "Please enter a valid phone number", Toast.LENGTH_SHORT).show();
                } else {
                    loginHelper();
                }
            }
        });
        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName;
                userName = name.getText().toString();
                mUserPassword = password.getText().toString();
                mUserPhonenumber = phoneNumber.getText().toString();
                if (userName.trim().length() < 2) {
                    Toast.makeText(getBaseContext(), "Please Enter Your Name", Toast.LENGTH_SHORT).show();
                } else if (mUserPassword.trim().length() < 6) {
                    Toast.makeText(getBaseContext(), "Please enter a password", Toast.LENGTH_SHORT).show();
                } else if (mUserPhonenumber.trim().length() < 8) {
                    Toast.makeText(getBaseContext(), "Please enter a valid phone number", Toast.LENGTH_SHORT).show();
                } else {
                    //create user
                    //get shared preferences
                    sharedPref = getPreferences(Context.MODE_PRIVATE);
                    BackendlessUser mUser = new BackendlessUser();
                    mUser.setProperty("name", userName);
                    mUser.setProperty("phonenumber", mUserPhonenumber);
                    mUser.setPassword(mUserPassword);
                    SharedPreferences.Editor mEdit = sharedPref.edit();
                    mEdit.putString(PHONENUMBER, mUserPhonenumber);
                    mEdit.putString(PASSWORD, mUserPassword);
                    mEdit.commit();
                    Backendless.UserService.register(mUser, new BackendlessCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser response) {
                            Toast.makeText(getBaseContext(), "Registration Successful!", Toast.LENGTH_SHORT).show();

                            loginHelper();
                        }
                    });
                }
            }
        });

    }
    /********************LOGIN AND SIGNUP************************************/
    /**
     * Register user if unregistered, login if logged out, show map if logged in
     ***/
    public void checkLogin() {

        setUpLoginAndSignup();
        //get shared preferences
        sharedPref = getPreferences(Context.MODE_PRIVATE);

        //check for existing id
        mUserPhonenumber = sharedPref.getString(PHONENUMBER, "");
        mUserPassword = sharedPref.getString(PASSWORD, "");
        String currentUserObjectId = Backendless.UserService.loggedInUser();
        Backendless.UserService.findById(currentUserObjectId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Backendless.UserService.setCurrentUser(user);
                currentUser = user;
                navUsername = (TextView) findViewById(R.id.nav_username);
                navUsername.setText(currentUser.getProperty("name").toString());
                checkforPackages();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                System.out.println("Error finding user by ID");
                makeViewVisible(login);
            }
        });


        //if no existing id, create new one, then save it, create new user
//        if (mUserPhonenumber.equals("") || mUserPassword.equals("")) {
//            mUserID = UUID.randomUUID().toString();
//            System.out.println("mUserID inside = " + mUserPhonenumber);
//
//
//            makeViewVisible(signup);
//        } else {
//
//            loginHelper();
//        }
    }

    //actually log in
    public void loginHelper() {

        Backendless.UserService.login(mUserPhonenumber, mUserPassword, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                //logged in, show map activity
                if (login.getVisibility() == View.VISIBLE) {
                    makeViewInvisible(login);
                }
                if (signup.getVisibility() == View.VISIBLE) {
                    makeViewInvisible(signup);
                }
                currentUser = Backendless.UserService.CurrentUser();
                Toast.makeText(getBaseContext(), "Login Successful!", Toast.LENGTH_SHORT).show();
                makeViewVisible(mapFragment.getView());
                //setUpMap();
                navUsername = (TextView) findViewById(R.id.nav_username);
                navUsername.setText(currentUser.getProperty("name").toString());
                checkforPackages();

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                System.out.println("Error loggin in");
                makeViewVisible(signup);
            }
        }, true); //set stay logged in to true
    }


    /*****************************
     * END OF LOGIN AND SIGNUP
     *****************************************/


    private void setUpLoginAndSignup() {
        login = (ScrollView) findViewById(R.id.layout_login);
        signup = (ScrollView) findViewById(R.id.layout_signup);

    }

    /**
     * Set up backeless and connect
     **/

    private void setUpBackendless() {
        String appVersion = "v1";
        Backendless.initApp(this, "AD95965E-DDD3-F314-FFD3-D114AEEA7800", "6A4B8939-A042-DC03-FFCF-8D1704E61900", appVersion);
    }


    /***
     * Track the user's location
     **/
    private void setUpRealTimeLocation(Bundle savedInstanceState) {
        mLastUpdateTime = "";
        // Update values using data stored in the Bundle.
        updateValuesFromBundle(savedInstanceState);

        // Kick off the process of building a GoogleApiClient and requesting the LocationServices
        // API.
        buildGoogleApiClient();
    }

    /*****************Realtime location*************/
    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {


            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                // Since LOCATION_KEY was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(LAST_UPDATED_TIME_STRING_KEY)) {
                mLastUpdateTime = savedInstanceState.getString(LAST_UPDATED_TIME_STRING_KEY);
            }
            setUpMap();
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestLocationPermission();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.

        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    protected void onStart() {
        super.onStart();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        mGoogleApiClient.connect();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onResume() {
        super.onResume();
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.

        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();

        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");

        // If the initial location was never previously requested, we use
        // FusedLocationApi.getLastLocation() to get it. If it was previously requested, we store
        // its value in the Bundle and check for it in onCreate(). We
        // do not request it again unless the user specifically requests location updates by pressing
        // the Start Updates button.
        //
        // Because we cache the value of the initial location in the Bundle, it means that if the
        // user launches the activity,
        // moves to a new location, and then changes the device orientation, the original location
        // is displayed as the activity is re-created.
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                Log.i(TAG, "Permissions fail");
                requestLocationPermission();
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

            setUpMap();

        }

        // If the user presses the Start Updates button before GoogleApiClient connects, we set
        // mRequestingLocationUpdates to true (see startUpdatesButtonHandler()). Here, we check
        // the value of mRequestingLocationUpdates and if it is true, we start location updates.

        startLocationUpdates();

    }


    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(final Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        if (isDelivering) {
            Backendless.Persistence.of(Deliveries.class).findById(deliveryId, new AsyncCallback<Deliveries>() {
                @Override
                public void handleResponse(Deliveries response) {
                    if (response.getDeliverStatus() == 0) {
                        response.setLocationOfPackage(mCurrentLocation.getLatitude() + " !! " + mCurrentLocation.getLongitude());


                        Backendless.Persistence.save(response, new AsyncCallback<Deliveries>() {
                            @Override
                            public void handleResponse(Deliveries response2) {
                                //upDateRecipientHome();
                                Toast.makeText(getBaseContext(), "Location sent to recipient", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {

                            }
                        });
                    } else {

                        checkforPackages();
                        isDelivering = false;
                    }

                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Toast.makeText(getBaseContext(), "Delivery not found", Toast.LENGTH_SHORT).show();
                    isDelivering = false;

                    checkforPackages();
                }
            });
            setUpMap();

        } else {

            checkforPackages();
        }

        Toast.makeText(this, "Location changed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, true);
        savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
        savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    /**********************End of realtime location**********************/

    /***
     * set up merchant list
     */
    private void setUpMerchantList() {
        //set up merchant list


        listView = (ListView) findViewById(R.id.merchant_list);
        //listView.setAdapter(adapter);

        CustomList adapter = new CustomList(this, merchants, imageId);
        listView = (ListView) findViewById(R.id.merchant_list);


        listView.setAdapter(adapter);

        callMerchant = (Button) findViewById(R.id.button_call_merchant);
        placeOrder = (Button) findViewById(R.id.button_place_order);
    }


    /**
     * Map set up
     */
    public void setUpMap() {
        // Initializing

        markerPoints = new ArrayList<LatLng>();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    /**
     * Set up action bar and navigation/
     */
    private void setUpActionBarAndNavigation() {
        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);


        // Find our drawer view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        // Find our drawer view
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        // Setup drawer view
        setupDrawerContent(nvDrawer);
        drawerToggle = setupDrawerToggle();

        // Tie DrawerLayout events to the ActionBarToggle
        mDrawer.addDrawerListener(drawerToggle);
        setUpOnClickListeners();


    }

    /***
     * Set up on click listeners for:
     */

    private void setUpOnClickListeners() {
        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(getApplicationContext(), "Order ", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        callMerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(getApplicationContext(), "Call ", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast toast = Toast.makeText(getApplicationContext(), merchants[position], Toast.LENGTH_SHORT);
                toast.show();
//                if (merchants[position] == "Jumia") {
//                    makeViewVisible(callMerchant);
//                    makeViewVisible(placeOrder);
//                }
                if (merchants[position] == "Jumia") {
                    goToUrl("https://www.jumia.co.ke/");
                } else if (merchants[position] == "Kilimall") {
                    goToUrl("https://www.kilimall.co.ke/");
                } else if (merchants[position] == "Shop It") {
                    goToUrl("http://shopit.co.ke/");
                } else if (merchants[position] == "Mama Mikes") {
                    goToUrl("http://mamamikes.co.ke/index.php?");
                } else if (merchants[position] == "Mimi") {
                    goToUrl("http://mimi.co.ke/");
                } else if (merchants[position] == "Electro Hub") {
                    goToUrl("http://electrohub.co.ke/");
                }

            }
        });

        listViewDeliveryLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                deliveryLocationsDialog(position);

            }
        });



    }

    private void deliveryLocationsDialog(final int position){
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Share");
        arrayAdapter.add("Set as preferred delivery location");
        arrayAdapter.add("Delete");

        builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);

                if(strName.equals("Share")){
                    //share via most messaging apps
                }
                else if(strName.equals("Set as preferred delivery location")){
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(MapsActivity.this);
                    builderInner.setMessage("Set this location as your preferred delivery location?");
                    builderInner.setTitle("Set as preferred location");
                    builderInner.setIcon(R.mipmap.ic_launcher);
                    builderInner.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int which) {
                            //set this location as preferred delivery location
                            setAsPreferredDeliveryLocation(deliveryLocationNamesArrayList.get(position), position);
                        }
                    });
                    builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();
                }
                else if(strName.equals("Delete")){

                }

            }
        });
        builderSingle.show();
    }


    private void setAsPreferredDeliveryLocation(String deliveryLocationName, int position){
        String currentUserObjectId = Backendless.UserService.loggedInUser();
        final String locationNameFinal = deliveryLocationName, deliveryLocationCoordinates = deliveryLocationCoordinatesArrayList.get(position);
        Backendless.UserService.findById(currentUserObjectId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Backendless.UserService.setCurrentUser(user);
                user.setProperty("deliverylocation", deliveryLocationCoordinates);
                user.setProperty("locationName", locationNameFinal);
                Backendless.Persistence.save(user, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {

                        makeViewVisible(mapFragment.getView());
                        Toast.makeText(getBaseContext(), "Delivery location has been set", Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getBaseContext(), "Attempt to set delivery location failed. Please try again", Toast.LENGTH_SHORT);
                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                makeViewVisible(signup);
            }
        });
    }

    private void goToUrl(String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }


    public void selectDrawerItem(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_map:
                makeViewInvisible(listView);
                makeDeliveryLocationListInvisible();
                makeViewVisible(mapFragment.getView());
                System.out.println("Nav Map");
                break;
            case R.id.nav_merchants:
                System.out.println("Merchants");
                makeDeliveryLocationListInvisible();
                makeViewInvisible(mapFragment.getView());
                makeViewVisible(listView);
                break;
            case R.id.nav_logout:
                logout();
                break;
            case R.id.nav_deliverylocation:
                //setDeliveryLocation();

                queryDeliveryLocations();
                makeViewInvisible(listView);
                makeViewInvisible(mapFragment.getView());
                makeDeliveryLocationListVisible();
                break;
            default:
                //show map
                makeViewVisible(mapFragment.getView());
        }


        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }




    /**
     * Logs user out
     **/
    private void logout() {
        Backendless.UserService.logout(new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                Toast.makeText(getBaseContext(), "Logged out", Toast.LENGTH_LONG);
                makeViewVisible(login);
                password_login.setText("");
                makeViewInvisible(mapFragment.getView());
                makeViewInvisible(listView);

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(getBaseContext(), "Log out attempt failed. Please try again", Toast.LENGTH_LONG);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        //not sure if code below should be around
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        //end of ish ish code
        return super.onOptionsItemSelected(item);
    }


    // `onPostCreate` called when activity start-up is complete after `onStart()`
    // NOTE! Make sure to override the method with only a single `Bundle` argument
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Error downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Maps Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }


    /**
     * Fetches data from url passed
     **/
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        /**
         * Executes in UI thread, after the parsing process
         */
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(2);
                lineOptions.color(Color.BLUE).width(10);
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions);
            } else {
                Toast.makeText(getBaseContext(), "Directions cannot be found! Try again", Toast.LENGTH_SHORT).show();
                setUpOrigin();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        showDirections();

    }


    public void makeViewVisible(View v) {

        v.setVisibility(View.VISIBLE);
    }

    public void makeViewInvisible(View v) {

        v.setVisibility(View.GONE);
    }


    public void showDirections() {
        if (mCurrentLocation == null) {
            return;
        } else {
            if (!isDelivering) {
                setUpOrigin();
            } else {
                setUpDestinationAndDirections();
            }
        }


    }


    private boolean hasPackages = false;

    private void checkforPackages() {
        if (mMap != null) {
            mMap.clear();
            System.out.println("Cleared!!!");
        }
        String currentUserObjectId = Backendless.UserService.loggedInUser();
        Backendless.UserService.findById(currentUserObjectId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Backendless.UserService.setCurrentUser(user);
                currentUser = user;
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                makeViewVisible(signup);
            }
        });
        if (currentUser != null) {

            String whereClause = "deliverStatus = 0 AND DeliveryRecipient = " + currentUser.getProperty("phonenumber").toString();
            BackendlessDataQuery dataQuery = new BackendlessDataQuery();


            dataQuery.setWhereClause(whereClause);


            Backendless.Persistence.of(Deliveries.class).find(dataQuery, new AsyncCallback<BackendlessCollection<Deliveries>>() {
                @Override
                public void handleResponse(BackendlessCollection<Deliveries> deliveries) {
                    System.out.println("These are the deliveries #: " + deliveries.getTotalObjects());

                    if (deliveries.getTotalObjects() < 1) {
                        setUpOrigin();
                        return;
                    }
                    hasPackages = true;
                    Iterator<Deliveries> iterator = deliveries.getCurrentPage().iterator();

                    while (iterator.hasNext()) {

                        Deliveries deliveryItr = iterator.next();
                        String[] coodinates = deliveryItr.getLocationOfPackage().split("!!");

                        mPackageLocation.setLatitude(Double.parseDouble(coodinates[0]));
                        mPackageLocation.setLongitude(Double.parseDouble(coodinates[1]));

                        setUpPackageLocations();

                    }
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    setUpMap();
                }
            });
        } else {
            System.out.println("User not logged in");
            checkLogin();
        }

    }


    /***
     * sets up origin marker and name
     **/
    private void setUpOrigin() {
        //checkforPackages();


        mMap.clear();
        origin = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        MarkerOptions originOptions = new MarkerOptions();
        originOptions.position(origin);

        originOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(origin, 13));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(origin)      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestLocationPermission();
            return;
        }
        mMap.setMyLocationEnabled(true);
        Toast.makeText(getBaseContext(), "You have no active deliveries", Toast.LENGTH_LONG);

    }


    public void setUpPackageLocations() {
        origin = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(origin, 13));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(origin)      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestLocationPermission();
            return;
        }
        mMap.setMyLocationEnabled(true);


        /*******************/
        //Creating MarkerOptions
        dest = new LatLng(mPackageLocation.getLatitude(), mPackageLocation.getLongitude());
        MarkerOptions destinationOptions = new MarkerOptions();

        destinationOptions.position(dest);

        mMap.addMarker(destinationOptions).setTitle("Delivery");

        destinationOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        LatLngBounds.Builder builder = new LatLngBounds.Builder();


        builder.include(origin);
        builder.include(dest);

        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        //CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.moveCamera(cameraUpdate);

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, dest);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);


    }

    /****
     * Sets up target marker
     *********/
    private void setUpDestinationAndDirections() {
        makeViewVisible(buttonPackageDelivered);
        makeViewVisible(startNavigation);
        setUpOrigin();

        //Creating MarkerOptions
        dest = new LatLng(mTargetLocation.getLatitude(), mTargetLocation.getLongitude());
        MarkerOptions destinationOptions = new MarkerOptions();

        destinationOptions.position(dest);

        mMap.addMarker(destinationOptions);

        destinationOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        LatLngBounds.Builder builder = new LatLngBounds.Builder();


        builder.include(origin);
        builder.include(dest);

        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20); // offset from edges of the map 20% of screen

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        //CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.moveCamera(cameraUpdate);

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, dest);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }


    private void setDeliveryStatus() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delivery Status");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Has the package been delivered successfully to the recipient?");

// Set up the buttons
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Backendless.Persistence.of(Deliveries.class).findById(deliveryId, new AsyncCallback<Deliveries>() {
                    @Override
                    public void handleResponse(Deliveries response) {

                        response.setDeliverStatus(1);


                        Backendless.Persistence.save(response, new AsyncCallback<Deliveries>() {
                            @Override
                            public void handleResponse(Deliveries response2) {
                                //upDateRecipientHome();


                                isDelivering = false;
                                Toast.makeText(getBaseContext(), "Delivery s/tatus has been updated", Toast.LENGTH_SHORT).show();
                                makeViewInvisible(startNavigation);
                                makeViewInvisible(buttonPackageDelivered);
                                checkforPackages();
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {

                            }
                        });
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getBaseContext(), "Delivery not found", Toast.LENGTH_SHORT).show();
                        isDelivering = false;

                        checkforPackages();
                    }
                });

            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    private BackendlessUser deliveryRecipient;

    private void findRecipient() {
        if (recipientPhoneNumber.trim().length() < 6) {
            Toast.makeText(getBaseContext(), "Phone number not valid. Try again", Toast.LENGTH_LONG);
            return;
        }
        String whereClause = "phonenumber = " + recipientPhoneNumber;
        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        QueryOptions queryOptions = new QueryOptions();
        //retrieve relations as well
        queryOptions.addRelated("deliverylocation");
        queryOptions.addRelated("Places");
        dataQuery.setQueryOptions(queryOptions);

        dataQuery.setWhereClause(whereClause);


        Backendless.Persistence.of(BackendlessUser.class).find(dataQuery, new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
            String message = "It seems like the user does not have Prava installed. Try calling them";

            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> user) {
                Iterator<BackendlessUser> iterator = user.getCurrentPage().iterator();

                if (iterator.hasNext()) {

                    try {
                        deliveryRecipient = iterator.next();
                        createDelivery();
                    } catch (NoSuchElementException e) {
                        Toast.makeText(getBaseContext(), "User not found!", Toast.LENGTH_LONG).show();

                        showCallRecipientDialog(message);
                    }
                } else {

                    Toast.makeText(getBaseContext(), "User not found!", Toast.LENGTH_LONG).show();
                    showCallRecipientDialog(message);

                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(getBaseContext(), "User not found!", Toast.LENGTH_LONG).show();
                showCallRecipientDialog(message);
            }
        });


    }

    private String deliveryId;

    /*****
     * Create and save delivery object
     *****/
    private void createDelivery() {
        Deliveries delivery = new Deliveries();
        delivery.setLocationOfPackage(mCurrentLocation.getLatitude() + " !! " + mCurrentLocation.getLongitude());
        delivery.setDeliveryPerson(currentUser.getProperty("phonenumber").toString());
        delivery.setDeliveryRecipient(recipientPhoneNumber);
        // save object asynchronously
        Backendless.Persistence.save(delivery, new AsyncCallback<Deliveries>() {
            public void handleResponse(Deliveries response) {
                deliveryId = response.getObjectId();
                // new Deliveries instance has been saved
                findDeliveryLocation();
            }

            public void handleFault(BackendlessFault fault) {
                // an error has occurred, the error code can be retrieved with fault.getCode()
            }
        });
    }

    private void upDateRecipientHome() {

        Backendless.Persistence.of(BackendlessUser.class).findById(deliveryRecipient.getObjectId(), new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser recipient) {
                String[] recipientHome = recipient.getProperty("deliverylocation").toString().split("!!");
                mTargetLocation.setLatitude(Double.parseDouble(recipientHome[0]));
                mTargetLocation.setLongitude(Double.parseDouble(recipientHome[1]));
                setUpDestinationAndDirections();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(getBaseContext(), "Recipient not found!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isDelivering = false;

    /*********
     * Find the recipients delivery location
     ************/
    private void findDeliveryLocation() {
        try {
            String deliveryLocation = deliveryRecipient.getProperty("deliverylocation").toString();
            if (deliveryLocation != null || deliveryLocation.trim().length() > 5) {
                String latLong[] = deliveryLocation.split("!!");
                mTargetLocation = new Location(LocationManager.GPS_PROVIDER);
                mTargetLocation.setLatitude(Double.parseDouble(latLong[0]));
                mTargetLocation.setLongitude(Double.parseDouble(latLong[1]));
                if (listView.getVisibility() == View.VISIBLE) {
                    makeViewInvisible(listView);
                }
                makeViewVisible(buttonPackageDelivered);
                makeViewVisible(startNavigation);
                System.out.println("start navigation is showing");
                isDelivering = true;
                setUpMap();
            }
        } catch (NullPointerException e) {
            Toast.makeText(getBaseContext(), "Recipient has not set a delivery location ", Toast.LENGTH_LONG).show();
            Toast.makeText(getBaseContext(), "Try calling them", Toast.LENGTH_LONG).show();
            //showdialog to call;
            //or can track where they at rn;;
            String message = "Recipient has not set a delivery location. Try calling them";
            showCallRecipientDialog(message);
        }


    }

    private void showCallRecipientDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Call " + recipientPhoneNumber + "?");
        builder.setMessage(message);

// Set up the buttons
        builder.setPositiveButton("Call", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + recipientPhoneNumber));
                if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    requestCallPermission();
                    return;
                }
                startActivity(intent);


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    private void requestCallPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mapFragment.getView().getVisibility() == View.VISIBLE ||
                login.getVisibility() == View.VISIBLE || signup.getVisibility() == View.VISIBLE) {
            super.onBackPressed();
        } else {
            makeViewVisible(mapFragment.getView());
            makeViewInvisible(listView);
        }
    }


    public void requestLocationPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                showLocationPermissionDialog("The app needs to use your location for deliveries", locationPermissionDialog);
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_NETWORK_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_NETWORK_STATE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                showLocationPermissionDialog("Give the app permission to make a call", callPermissionDialog);

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                        MY_PERMISSIONS_REQUEST_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    setUpMap();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    return;
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_CALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + recipientPhoneNumber));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        requestCallPermission();
                        return;
                    }
                    startActivity(intent);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void showLocationPermissionDialog(String message, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", listener)
                .setNegativeButton("Cancel", listener)
                .create()
                .show();
    }

    DialogInterface.OnClickListener locationPermissionDialog = new DialogInterface.OnClickListener() {

        final int BUTTON_NEGATIVE = -2;
        final int BUTTON_POSITIVE = -1;

        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case BUTTON_NEGATIVE:
                    // int which = -2
                    dialog.dismiss();
                    MapsActivity.this.finish();
                    System.exit(0);
                    break;

                case BUTTON_POSITIVE:
                    // int which = -1
                    ActivityCompat.requestPermissions(
                            MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                    dialog.dismiss();
                    break;
            }
        }
    };

    DialogInterface.OnClickListener callPermissionDialog = new DialogInterface.OnClickListener() {

        final int BUTTON_NEGATIVE = -2;
        final int BUTTON_POSITIVE = -1;

        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case BUTTON_NEGATIVE:
                    // int which = -2
                    dialog.dismiss();
                    break;

                case BUTTON_POSITIVE:
                    // int which = -1
                    ActivityCompat.requestPermissions(
                            MapsActivity.this, new String[]{Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_CALL);
                    dialog.dismiss();
                    break;
            }
        }
    };

    String[] deliveryLocationNames;
    List<String> deliveryLocationNamesArrayList = new ArrayList<>();
    List<String> deliveryLocationCoordinatesArrayList = new ArrayList<>();
    /*Query and retrieve this user's saved delivery locations*/
    public void queryDeliveryLocations(){
        deliveryLocationNamesArrayList.clear(); //clear array to make room for new and get rid of old to avoid duplicity bug
        String currentUserObjectId = Backendless.UserService.loggedInUser();
        String whereClause = "ownerId = '" + currentUserObjectId +"'";
        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        dataQuery.setWhereClause( whereClause );

        // ***********************************************************
        // Asynchronous API:
        // ***********************************************************
        Backendless.Persistence.of( DeliveryLocations.class ).find( dataQuery,
                new AsyncCallback<BackendlessCollection<DeliveryLocations>>(){
                    @Override
                    public void handleResponse( BackendlessCollection<DeliveryLocations> deliveryLocations)
                    {
                        Iterator<DeliveryLocations> iterator = deliveryLocations.getCurrentPage().iterator();

                        while (iterator.hasNext()) {

                            DeliveryLocations deliveryLocationsItr = iterator.next();
                            deliveryLocationNamesArrayList.add(deliveryLocationsItr.getLocationName());
                            deliveryLocationCoordinatesArrayList.add(deliveryLocationsItr.getLocationCordinates());

                        }
                        System.out.println("These are tje locations: " + deliveryLocationNamesArrayList.size());
                        deliveryLocationNames = deliveryLocationNamesArrayList.toArray(new String[deliveryLocationNamesArrayList.size()]);
                        setUpDeliveryLocationsList();
                    }
                    @Override
                    public void handleFault( BackendlessFault fault )
                    {
                        // an error has occurred, the error code can be retrieved with fault.getCode()
                        System.out.println("Error!!!!!!!!!!!!!!!!");
                        System.out.println(fault.getCode());


                    }
                });
    }


    /***
     * set up delivery locations list
     */
    private void setUpDeliveryLocationsList() {



        CustomDeliveryLocationsList adapter = new CustomDeliveryLocationsList(this, deliveryLocationNames, deliveryLocationNames);
        listViewDeliveryLocations = (ListView) findViewById(R.id.delivery_locations_list);

        listViewDeliveryLocations.setAdapter(adapter);
    }


    /***
     * Set up add delivery location button
     * **********/
    private void setUpAddDeliveryButton() {
        buttonAddDeliveryLocation = (Button) findViewById(R.id.button_add_delivery_location);

        buttonAddDeliveryLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddDeliveryLocationDialog();

            }
        });
    }

    private void showAddDeliveryLocationDialog() {
        final AlertDialog.Builder addDeliveryLocationDialog = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);
        edittext.setHint("Location Name");
        String message = "Add this location to your delivery locations?", title = "Save Location";
        addDeliveryLocationDialog.setMessage(message);
        addDeliveryLocationDialog.setTitle(title);
        addDeliveryLocationDialog.setIcon(R.mipmap.ic_launcher);

        addDeliveryLocationDialog.setView(edittext);

        addDeliveryLocationDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String deliveryLocationName = edittext.getText().toString();
                System.out.println("This is the message: " + deliveryLocationName);
                if(deliveryLocationName.trim().length() == 0){
                    Toast.makeText(getBaseContext(), "Please add a name for the location", Toast.LENGTH_SHORT).show();

                }else{
                    setDeliveryLocation(deliveryLocationName);
                }

            }
        });

        addDeliveryLocationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
                dialog.dismiss();
            }
        });

        addDeliveryLocationDialog.show();
    }

    private void setDeliveryLocation(String locationName) {
        DeliveryLocations deliveryLocationToAdd = new DeliveryLocations();
        deliveryLocationToAdd.setLocationName(locationName);
        deliveryLocationToAdd.setLocationCordinates(mCurrentLocation.getLatitude() + " !! " + mCurrentLocation.getLongitude());
        // save object asynchronously
        Backendless.Persistence.save( deliveryLocationToAdd, new AsyncCallback<DeliveryLocations>() {
            public void handleResponse( DeliveryLocations response )
            {

                queryDeliveryLocations();
                Toast.makeText(getBaseContext(), "Delivery location has been set", Toast.LENGTH_SHORT);
            }

            public void handleFault( BackendlessFault fault )
            {
                // an error has occurred, the error code can be retrieved with fault.getCode()
                Toast.makeText(getBaseContext(), "Attempt to set delivery location failed. Please try again", Toast.LENGTH_SHORT);
            }
        });

    }

    public void initializeDeliveryLocationsListView(){
        listViewDeliveryLocations = (ListView) findViewById(R.id.delivery_locations_list);
        buttonAddDeliveryLocation = (Button) findViewById(R.id.button_add_delivery_location);
        setUpAddDeliveryButton();
    }

    public void makeDeliveryLocationListVisible(){
        makeViewVisible(listViewDeliveryLocations);
        makeViewVisible(buttonAddDeliveryLocation);
    }

    public void makeDeliveryLocationListInvisible(){
        makeViewInvisible(listViewDeliveryLocations);
        makeViewInvisible(buttonAddDeliveryLocation);
    }

}