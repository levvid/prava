package com.mlginc.gibsonlevvid.prava;

/**
 * Created by Gibson Levvid on 8/1/2016.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomList extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] companyName;

    public CustomList(Activity context,
                      String[] companyName, Integer[] imageId) {
        super(context, R.layout.activity_listview, companyName);
        this.context = context;
        this.companyName = companyName;


    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.activity_listview, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);


        txtTitle.setText(companyName[position]);


        return rowView;
    }
}
