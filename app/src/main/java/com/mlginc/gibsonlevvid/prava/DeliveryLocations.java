package com.mlginc.gibsonlevvid.prava;

/**
 * Created by Gibson Levvid on 2/28/2017.
 */


import com.backendless.Backendless;

public class DeliveryLocations
{
    private String locationName;
    private String locationDescription;
    private java.util.Date created;
    private String objectId;
    private java.util.Date updated;
    private String ownerId;
    private String locationCordinates;

    public String getLocationName()
    {
        return this.locationName;
    }

    public String getLocationDescription()
    {
        return this.locationDescription;
    }

    public java.util.Date getCreated()
    {
        return this.created;
    }

    public String getObjectId()
    {
        return this.objectId;
    }

    public java.util.Date getUpdated()
    {
        return this.updated;
    }

    public String getOwnerId()
    {
        return this.ownerId;
    }

    public String getLocationCordinates()
    {
        return this.locationCordinates;
    }


    public void setLocationName( String locationName )
    {
        this.locationName = locationName;
    }

    public void setLocationDescription( String locationDescription )
    {
        this.locationDescription = locationDescription;
    }

    public void setCreated( java.util.Date created )
    {
        this.created = created;
    }

    public void setObjectId( String objectId )
    {
        this.objectId = objectId;
    }

    public void setUpdated( java.util.Date updated )
    {
        this.updated = updated;
    }

    public void setOwnerId( String ownerId )
    {
        this.ownerId = ownerId;
    }

    public void setLocationCordinates( String locationCordinates )
    {
        this.locationCordinates = locationCordinates;
    }

    public DeliveryLocations save()
    {
        return Backendless.Data.of( DeliveryLocations.class ).save( this );
    }

    public Long remove()
    {
        return Backendless.Data.of( DeliveryLocations.class ).remove( this );
    }

    public static DeliveryLocations findById( String id )
    {
        return Backendless.Data.of( DeliveryLocations.class ).findById( id );
    }

    public static DeliveryLocations findFirst()
    {
        return Backendless.Data.of( DeliveryLocations.class ).findFirst();
    }

    public static DeliveryLocations findLast()
    {
        return Backendless.Data.of( DeliveryLocations.class ).findLast();
    }
}
